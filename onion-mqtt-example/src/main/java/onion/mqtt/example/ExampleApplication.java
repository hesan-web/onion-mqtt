package onion.mqtt.example;

import onion.mqtt.server.MqttServer;
import onion.mqtt.server.MqttServerBuilder;
import org.noear.solon.Solon;
import org.noear.solon.annotation.SolonMain;

/**
 * @author Mr, Lu
 * @developmentTeam 浙江允泽信息科技有限公司
 * @createTime 2023/12/14
 */
@SolonMain
public class ExampleApplication {
    public static void main(String[] args){
        Solon.start(ExampleApplication.class, args, app -> {
            MqttServer server = new MqttServerBuilder().config(null).build();
            server.start();
        });
    }
}
