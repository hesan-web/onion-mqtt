package onion.mqtt.server.processor;

import io.netty.channel.Channel;
import io.netty.handler.codec.mqtt.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Mr, Lu
 * @developmentTeam 浙江允泽信息科技有限公司
 * @createTime 2023/12/13
 */
public class PubCompProcessor extends AbstractMqttServerProcessor<MqttMessage> {
    static final Logger log = LoggerFactory.getLogger(PubCompProcessor.class);
    @Override
    public void process(Channel channel, MqttMessage message) {
        log.debug("PubComp clientId: {}", getClientId(channel));
    }
}
