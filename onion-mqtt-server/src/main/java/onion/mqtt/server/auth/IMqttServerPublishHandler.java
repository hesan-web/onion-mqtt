package onion.mqtt.server.auth;

import io.netty.channel.Channel;
import io.netty.handler.codec.mqtt.MqttQoS;

/**
 * @author Mr, Lu
 * @developmentTeam 浙江允泽信息科技有限公司
 * @createTime 2023/12/13
 */
public interface IMqttServerPublishHandler {

    /**
     * 否有发布权限
     *
     * @param ctx  Channel
     * @param clientId 客户端 id
     * @param topic    topic
     * @param qoS      MqttQoS
     * @param isRetain 是否保留消息
     * @return 否有发布权限
     */
    default boolean verifyPermission(Channel ctx, String clientId, String topic, MqttQoS qoS, boolean isRetain) {
        try {
            return hasPermission(ctx, clientId, topic, qoS, isRetain);
        } catch (Throwable e) {
            return false;
        }
    }

    /**
     * 否有发布权限
     *
     * @param ctx  Channel
     * @param clientId 客户端 id
     * @param topic    topic
     * @param qoS      MqttQoS
     * @param isRetain 是否保留消息
     * @return 否有发布权限
     */
    boolean hasPermission(Channel ctx, String clientId, String topic, MqttQoS qoS, boolean isRetain);
}
