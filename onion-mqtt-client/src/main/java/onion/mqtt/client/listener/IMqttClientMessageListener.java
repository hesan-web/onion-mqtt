package onion.mqtt.client.listener;

import io.netty.channel.Channel;
import io.netty.handler.codec.mqtt.MqttPublishMessage;
import io.netty.handler.codec.mqtt.MqttQoS;

/**
 * @author Mr, Lu
 * @developmentTeam 浙江允泽信息科技有限公司
 * @createTime 2023/12/16
 */
public interface IMqttClientMessageListener {

    void onMessage(Channel channel, String topic, MqttQoS qoS, MqttPublishMessage message);
}
