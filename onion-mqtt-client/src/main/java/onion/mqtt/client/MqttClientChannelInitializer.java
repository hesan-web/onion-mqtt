package onion.mqtt.client;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.mqtt.MqttDecoder;
import io.netty.handler.codec.mqtt.MqttEncoder;

/**
 * @author Mr, Lu
 * @developmentTeam 浙江允泽信息科技有限公司
 * @createTime 2023/12/14
 */
public class MqttClientChannelInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        MqttClientInboundHandler handler = new MqttClientInboundHandler();
        //客户端初始化
        socketChannel.pipeline().addLast("decoder", new MqttDecoder(1024 * 8));
        socketChannel.pipeline().addLast("encoder", MqttEncoder.INSTANCE);
        socketChannel.pipeline().addLast(handler);
    }
}
