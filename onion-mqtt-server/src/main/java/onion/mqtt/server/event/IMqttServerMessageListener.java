package onion.mqtt.server.event;

import io.netty.channel.Channel;
import io.netty.handler.codec.mqtt.MqttPublishMessage;
import io.netty.handler.codec.mqtt.MqttQoS;

/**
 * 消息事件接口
 *
 * @author Mr, Lu
 * @developmentTeam 浙江允泽信息科技有限公司
 * @createTime 2023/12/13
 */
public interface IMqttServerMessageListener {

    /**
     * 监听到消息
     *
     * @param channel  Channel
     * @param clientId clientId
     * @param topic    topic
     * @param qoS      MqttQoS
     * @param message  Message
     */
    void onMessage(Channel channel, String clientId, String topic, MqttQoS qoS, MqttPublishMessage message);
}
