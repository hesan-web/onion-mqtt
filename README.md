# JAVA语言开发的MQTT SERVER组件

---

`onion-mqtt` **低延迟**、**高性能**的 `mqtt` 物联网组件。更多使用方式详见： **onion-mqtt-example** 模块。

## 🍱 使用场景

- 物联网（云端 mqtt broker）
- 物联网（边缘端消息通信）
- 群组类 IM
- 消息推送
- 简单易用的 mqtt 客户端

## ✨ 功能
- [x] 支持 MQTT v3.1、v3.1.1 以及 v5.0 协议。
- [x] 支持 MQTT server 服务端。
- [x] 支持 MQTT 遗嘱消息。
- [x] 支持 MQTT 保留消息。
- [x] 支持自定义连接认证。
- [x] 支持自定义订阅认证。
- [x] 支持自定义发布认证。
- [x] 支持消息事件监听。
- [x] 支持订阅事件监听。
- [x] 支持连接事件监听。


## 🌱 待办
- [ ] 支持 http rest api
- [ ] 优化处理 mqtt session，以及支持 v5.0
- [ ] 基于 easy-rule + druid sql 解析，实现规则引擎
- [ ] 基于 redis stream 实现集群

## 🚨 默认端口

| 端口号 | 协议            | 说明                 |
| ------ | --------------- |--------------------|
| 1883   | tcp             | mqtt tcp 端口  （可配置） |


## 📦️ 依赖

### 服务端
```xml
<dependency>
  <groupId>cloud.thingslinker</groupId>
  <artifactId>onion-mqtt-server</artifactId>
  <version>${onion-mqtt.version}</version>
</dependency>
```

**配置详见**：[onion-mqtt-server 使用文档](onion-mqtt-server/README.md)

## 📝 文档


## 💡 参考vs借鉴

[//]: # (- [mica-mqtt]&#40;https://gitee.com/596392912/mica-mqtt&#41;)
- [socket.d](https://gitee.com/noear/socketd?_from=gitee_search)

[//]: # (- [iot-mqtt-server]&#40;https://gitee.com/recallcode/iot-mqtt-server&#41;)
