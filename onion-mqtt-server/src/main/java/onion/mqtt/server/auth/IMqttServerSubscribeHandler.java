package onion.mqtt.server.auth;

import io.netty.channel.Channel;
import io.netty.handler.codec.mqtt.MqttQoS;

/**
 * 订阅认证接口
 *
 * @author Mr, Lu
 * @developmentTeam 浙江允泽信息科技有限公司
 * @createTime 2023/12/12
 */
public interface IMqttServerSubscribeHandler {

    /**
     * 验证订阅topic
     *
     * @param channel
     * @param clientId
     * @param topic
     * @param qos
     * @return
     */
    default boolean verifyTopic(Channel channel, String clientId, String topic, MqttQoS qos) {
        try {
            return isValid(channel, clientId, topic, qos);
        } catch (Throwable e) {
            return false;
        }
    }

    /**
     * 验证
     *
     * @param channel
     * @param clientId
     * @param topic
     * @param qos
     * @return
     */
    boolean isValid(Channel channel, String clientId, String topic, MqttQoS qos);
}
