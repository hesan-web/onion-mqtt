package onion.mqtt.server.store;

import io.netty.channel.Channel;

import java.io.Serializable;

/**
 * @author Mr, Lu
 * @developmentTeam 浙江允泽信息科技有限公司
 * @createTime 2023/12/12
 */
public class SessionStore implements Serializable {

    private static final long serialVersionUID = 8522068648627429515L;

    /**
     * 连接客户端ID
     */
    private String clientId;

    /**
     * 清除标志
     */
    private boolean cleanSession;

    /**
     * 会话channel
     */
    private Channel channel;

    /**
     * 有效时间
     */
    private int expire;

    public SessionStore() {
    }

    public SessionStore(String clientId, boolean cleanSession, Channel channel, int expire) {
        this.clientId = clientId;
        this.cleanSession = cleanSession;
        this.channel = channel;
        this.expire = expire;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public boolean isCleanSession() {
        return cleanSession;
    }

    public void setCleanSession(boolean cleanSession) {
        this.cleanSession = cleanSession;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public int getExpire() {
        return expire;
    }

    public void setExpire(int expire) {
        this.expire = expire;
    }
}
