package onion.mqtt.server;

/**
 * 服务端配置
 *
 * @author Mr, Lu
 * @developmentTeam 浙江允泽信息科技有限公司
 * @createTime 2023/12/12
 */
public class MqttServerConfig {

    /**
     * 节点名称，通常用于分布式区分
     */
    private String nodeId = "onion-mqtt-server";

    /**
     * 主机地址
     */
    private String host;

    /**
     * 端口
     */
    private int port = 1883;

    /**
     * boss线程数，负责接受accept消息的线程数，通常1个线程即可，传0或复数则为1，大于5的数则为5
     */
    private int bossThread = 1;

    /**
     * work线程数，负责处理消息的线程数，通常是cpu的核心*2个数量，传0或复数则为1，大于100的数则为100
     */
    private int workThread = 0;

    /**
     * 心跳时间，单位秒，默认120秒
     */
    private int keepAlive = 120;

    /**
     * 最大消息长度，默认100K
     */
    private int maxBytesInMessage = 102400;

    /**
     * 是否开启调试模式
     */
    private boolean debug = false;

    /**
     * ssl配置
     */
    private Ssl ssl = new Ssl();

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getBossThread() {
        return bossThread;
    }

    public void setBossThread(int bossThread) {
        this.bossThread = bossThread;
    }

    public int getWorkThread() {
        return workThread;
    }

    public void setWorkThread(int workThread) {
        this.workThread = workThread;
    }

    public int getKeepAlive() {
        return keepAlive;
    }

    public void setKeepAlive(int keepAlive) {
        this.keepAlive = keepAlive;
    }

    public int getMaxBytesInMessage() {
        return maxBytesInMessage;
    }

    public void setMaxBytesInMessage(int maxBytesInMessage) {
        this.maxBytesInMessage = maxBytesInMessage;
    }

    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    public Ssl getSsl() {
        return ssl;
    }

    public void setSsl(Ssl ssl) {
        this.ssl = ssl;
    }


    static
    class Ssl {
        private boolean enabled = false;

        private String serverCertFile;

        private String keyFile;

        private String caCertFile;

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        public String getServerCertFile() {
            return serverCertFile;
        }

        public void setServerCertFile(String serverCertFile) {
            this.serverCertFile = serverCertFile;
        }

        public String getKeyFile() {
            return keyFile;
        }

        public void setKeyFile(String keyFile) {
            this.keyFile = keyFile;
        }

        public String getCaCertFile() {
            return caCertFile;
        }

        public void setCaCertFile(String caCertFile) {
            this.caCertFile = caCertFile;
        }
    }
}
