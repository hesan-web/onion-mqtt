package onion.mqtt.server.event;

import io.netty.channel.Channel;

/**
 * 连接状态事件接口
 *
 * @author Mr, Lu
 * @developmentTeam 浙江允泽信息科技有限公司
 * @createTime 2023/12/12
 */
public interface IMqttServerConnectListener {

    /**
     * 设备上线
     *
     * @param channel
     * @param clientId
     */
    void online(Channel channel, String clientId);

    /**
     * 设备下线
     *
     * @param channel
     * @param clientId
     */
    void offline(Channel channel, String clientId);
}
