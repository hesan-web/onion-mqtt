package onion.mqtt.server.event;

import io.netty.channel.Channel;
import io.netty.handler.codec.mqtt.MqttQoS;

/**
 * 订阅事件接口
 *
 * @author Mr, Lu
 * @developmentTeam 浙江允泽信息科技有限公司
 * @createTime 2023/12/13
 */
public interface IMqttServerSubscribeListener {

    /**
     * 订阅
     *
     * @param ctx     Channel
     * @param clientId    clientId
     * @param topicFilter topicFilter
     * @param mqttQoS     MqttQoS
     */
    void onSubscribe(Channel ctx, String clientId, String topicFilter, MqttQoS mqttQoS);

    /**
     * 取消订阅
     *
     * @param ctx     Channel
     * @param clientId    clientId
     * @param topicFilter topicFilter
     */
    void onUnsubscribe(Channel ctx, String clientId, String topicFilter);
}
