package onion.mqtt.server;

/**
 * @author Mr, Lu
 * @developmentTeam 浙江允泽信息科技有限公司
 * @createTime 2023/12/12
 */
public class MqttServerConst {

    /**
     * 版本号
     */
    public final static String VERSION = "1.0.0";

    public final static String CLIENT_ID = "clientId";
}
