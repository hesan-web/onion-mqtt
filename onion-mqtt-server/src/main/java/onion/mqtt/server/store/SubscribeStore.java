package onion.mqtt.server.store;

import io.netty.handler.codec.mqtt.MqttQoS;

import java.io.Serializable;

/**
 * @author Mr, Lu
 * @developmentTeam 浙江允泽信息科技有限公司
 * @createTime 2023/12/12
 */
public class SubscribeStore implements Serializable {

    private static final long serialVersionUID = 7945565723208719180L;

    /**
     * 客户端id
     */
    private String clientId;

    /**
     * 订阅topic
     */
    private String topic;

    /**
     * 消息质量
     */
    private MqttQoS mqttQoS;

    public SubscribeStore() {
    }

    public SubscribeStore(String clientId, String topic, MqttQoS mqttQoS) {
        this.clientId = clientId;
        this.topic = topic;
        this.mqttQoS = mqttQoS;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public MqttQoS getMqttQoS() {
        return mqttQoS;
    }

    public void setMqttQoS(MqttQoS mqttQoS) {
        this.mqttQoS = mqttQoS;
    }
}
