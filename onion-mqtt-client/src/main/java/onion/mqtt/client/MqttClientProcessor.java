package onion.mqtt.client;

import io.netty.channel.Channel;
import io.netty.handler.codec.mqtt.MqttConnAckMessage;
import io.netty.handler.codec.mqtt.MqttMessage;
import io.netty.handler.codec.mqtt.MqttPublishMessage;
import io.netty.handler.codec.mqtt.MqttSubAckMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Mr, Lu
 * @developmentTeam 浙江允泽信息科技有限公司
 * @createTime 2023/12/12
 */
public class MqttClientProcessor {
    static final Logger log = LoggerFactory.getLogger(MqttClientProcessor.class);

    /**
     * 处理连接确认消息
     *
     * @param channel
     * @param message
     */
    void processConnAck(Channel channel, MqttConnAckMessage message) {

    }

    /**
     * 处理订阅确认消息
     *
     * @param channel
     * @param message
     */
    void processSubAck(Channel channel, MqttSubAckMessage message) {

    }

    /**
     * 处理取消订阅确认消息
     *
     * @param channel
     * @param message
     */
    void processUnsubAck(Channel channel, MqttMessage message) {}

    /**
     * 处理发布消息
     *
     * @param channel
     * @param message
     */
    void processPublish(Channel channel, MqttPublishMessage message) {

    }

    /**
     * 处理发布确认消息
     *
     * @param channel
     * @param message
     */
    void processPubAck(Channel channel, MqttMessage message) {}

    /**
     * 处理发布接收消息
     *
     * @param channel
     * @param message
     */
    void processPubRec(Channel channel, MqttMessage message) {}

    /**
     * 处理发布释放消息
     *
     * @param channel
     * @param message
     */
    void processPubRel(Channel channel, MqttMessage message) {}

    /**
     * 处理发布完成消息
     *
     * @param channel
     * @param message
     */
    void processPubComp(Channel channel, MqttMessage message) {}
}
