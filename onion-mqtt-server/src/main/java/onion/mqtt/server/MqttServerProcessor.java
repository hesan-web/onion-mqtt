package onion.mqtt.server;

import io.netty.channel.Channel;
import io.netty.handler.codec.mqtt.*;
import onion.mqtt.server.processor.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Mr, Lu
 * @developmentTeam 浙江允泽信息科技有限公司
 * @createTime 2023/12/12
 */
public class MqttServerProcessor {
    static final Logger log = LoggerFactory.getLogger(MqttServerProcessor.class);
    private final MqttServerBuilder serverBuilder;

    public MqttServerProcessor(MqttServerBuilder serverBuilder) {
        this.serverBuilder = serverBuilder;
    }

    /**
     * 连接处理
     *
     * @param channel
     * @param message
     */
    public void connect(Channel channel, MqttConnectMessage message) {
        new ConnectProcessor(serverBuilder).process(channel, message);
    }

    /**
     * 断开连接处理
     *
     * @param channel
     */
    public void disconnect(Channel channel) {
        new DisConnectProcessor(serverBuilder).process(channel, null);
    }

    /**
     * 订阅Topic消息处理
     *
     * @param channel
     * @param message
     */
    public void subscribe(Channel channel, MqttSubscribeMessage message) {
        new SubscribeProcessor(serverBuilder).process(channel, message);
    }

    /**
     * 取消订阅Topic消息处理
     *
     * @param channel
     * @param message
     */
    public void unsubscribe(Channel channel, MqttUnsubscribeMessage message) {
        new UnSubscribeProcessor(serverBuilder).process(channel, message);
    }

    /**
     * 发布消息处理
     *
     * @param channel
     * @param message
     */
    public void publish(Channel channel, MqttPublishMessage message) {
        new PublishProcessor(serverBuilder).process(channel, message);
    }

    /**
     * 心跳请求处理
     *
     * @param channel
     */
    public void pingReq(Channel channel) {
        new PingReqProcessor().process(channel, null);
    }

    /**
     * 心跳响应处理
     *
     * @param channel
     */
    public void pingRes(Channel channel) {

    }

    /**
     * 发布确认处理，用于QoS1消息发布收到确认
     *
     * @param channel
     * @param message
     */
    public void pubAck(Channel channel, MqttMessage message) {
        new PubAckProcessor().process(channel, message);
    }

    /**
     * 发布接收处理，发布收到（保证交付第一步）
     *
     * @param channel
     * @param message
     */
    public void pubRec(Channel channel, MqttMessage message) {
        new PubRecProcessor().process(channel, message);
    }

    /**
     * 发布释放处理，发布释放（保证交付第二步）
     * @param channel
     * @param message
     */
    public void pubRel(Channel channel, MqttMessage message) {
        new PubRelProcessor().process(channel, message);
    }

    /**
     * 发布完成处理，QoS 2 消息发布完成（保证交互第三步）
     *
     * @param channel
     * @param message
     */
    public void pubComp(Channel channel, MqttMessage message) {
        new PubCompProcessor().process(channel, message);
    }
}
