package onion.mqtt.client;

import java.io.Serializable;

/**
 * @author Mr, Lu
 * @developmentTeam 浙江允泽信息科技有限公司
 * @createTime 2023/12/15
 */
public class MqttClientConfig implements Serializable {
    private static final long serialVersionUID = -2527920594493134204L;

    /**
     * 主机地址
     */
    private String host = "127.0.0.1";

    /**
     * 端口
     */
    private int port = 1883;

    private Ssl ssl = new Ssl();

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public Ssl getSsl() {
        return ssl;
    }

    public void setSsl(Ssl ssl) {
        this.ssl = ssl;
    }

    public static class Ssl {
        private boolean enabled = false;

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }
    }
}
