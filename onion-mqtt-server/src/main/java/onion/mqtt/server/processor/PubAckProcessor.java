package onion.mqtt.server.processor;

import io.netty.channel.Channel;
import io.netty.handler.codec.mqtt.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Qos1 消息发布确认处理器
 *
 * @author Mr, Lu
 * @developmentTeam 浙江允泽信息科技有限公司
 * @createTime 2023/12/13
 */
public class PubAckProcessor extends AbstractMqttServerProcessor<MqttMessage> {
    static final Logger log = LoggerFactory.getLogger(PubAckProcessor.class);
    @Override
    public void process(Channel channel, MqttMessage message) {
        log.debug("PubAck -- clientId: {}", getClientId(channel));
//        MqttMessageIdVariableHeader idVariableHeader = (MqttMessageIdVariableHeader) message.variableHeader();
//        int messageId = idVariableHeader.messageId();
//        MqttMessage mqttMessage = MqttMessageBuilders.pubAck()
//                .packetId(messageId)
//                .build();
//        writeAndFlush(channel, mqttMessage);
    }
}
