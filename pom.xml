<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>cloud.thingslinker</groupId>
    <artifactId>onion-parent</artifactId>
    <version>1.0.2</version>
    <packaging>pom</packaging>

    <!-- project description -->
    <name>${project.artifactId}</name>
    <description>Onion mqtt tools.</description>
    <url>https://gitee.com/hesan-web/onion-mqtt</url>

    <modules>
        <module>onion-mqtt-server</module>
        <module>onion-mqtt-example</module>
        <module>onion-mqtt-client</module>
        <module>onion-mqtt-common</module>
    </modules>

    <properties>
        <!-- java version -->
        <java.version>1.8</java.version>
        <maven-compiler-plugin>3.8.1</maven-compiler-plugin>
        <maven.compiler.source>8</maven.compiler.source>
        <maven.compiler.target>8</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <!---->
        <onion.mqtt.version>1.0.2</onion.mqtt.version>
        <netty.version>4.1.90.Final</netty.version>
        <commons-lang3.version>3.12.0</commons-lang3.version>
        <slf4j.version>2.0.9</slf4j.version>
        <junit5.version>5.9.1</junit5.version>
    </properties>

    <dependencyManagement>
        <dependencies>
            <!-- commons-lang3 -->
            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-lang3</artifactId>
                <version>${commons-lang3.version}</version>
            </dependency>
            <!-- netty -->
            <dependency>
                <groupId>io.netty</groupId>
                <artifactId>netty-all</artifactId>
                <version>${netty.version}</version>
            </dependency>
            <!-- tinylog 内存占用更小、性能更好，适合边缘设备 -->
            <dependency>
                <groupId>org.tinylog</groupId>
                <artifactId>slf4j-tinylog</artifactId>
                <version>${tinylog.version}</version>
            </dependency>
            <dependency>
                <groupId>org.tinylog</groupId>
                <artifactId>tinylog-impl</artifactId>
                <version>${tinylog.version}</version>
            </dependency>
            <!-- onion-mqtt-common -->
            <dependency>
                <groupId>cloud.thingslinker</groupId>
                <artifactId>onion-mqtt-common</artifactId>
                <version>${onion.mqtt.version}</version>
            </dependency>
            <!-- onion-mqtt-server -->
            <dependency>
                <groupId>cloud.thingslinker</groupId>
                <artifactId>onion-mqtt-server</artifactId>
                <version>${onion.mqtt.version}</version>
            </dependency>
            <!-- onion-mqtt-client -->
            <dependency>
                <groupId>cloud.thingslinker</groupId>
                <artifactId>onion-mqtt-client</artifactId>
                <version>${onion.mqtt.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <!-- licenses -->
    <licenses>
        <license>
            <name>Apache License, Version 2.0</name>
            <url>https://www.apache.org/licenses/LICENSE-2.0</url>
        </license>
    </licenses>

    <!-- developers description -->
    <developers>
        <developer>
            <name>Mr,Lu</name>
            <email>301093752@qq.com</email>
            <organization>ThingsLinker</organization>
            <organizationUrl>https://www.thingslinker.cloud</organizationUrl>
        </developer>
    </developers>

    <!-- scm -->
    <scm>
        <connection>scm:git:https://gitee.com/hesan-web/onion-mqtt.git</connection>
        <developerConnection>scm:git:https://gitee.com/hesan-web/onion-mqtt.git</developerConnection>
        <url>https://gitee.com/hesan-web/onion-mqtt</url>
        <tag>v${project.version}</tag>
    </scm>

    <build>
        <plugins>
            <!-- Compiler -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                    <encoding>UTF-8</encoding>
                </configuration>
            </plugin>
            <!-- Release -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <version>2.5.3</version>
                <configuration>
                    <autoVersionSubmodules>true</autoVersionSubmodules>
                    <useReleaseProfile>false</useReleaseProfile>
                    <releaseProfiles>release</releaseProfiles>
                    <goals>deploy</goals>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>ossrh</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <build>
                <plugins>
                    <!-- Source -->
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-source-plugin</artifactId>
                        <version>2.2.1</version>
                        <executions>
                            <execution>
                                <id>attach-sources</id>
                                <goals>
                                    <goal>jar</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <!-- Javadoc -->
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-javadoc-plugin</artifactId>
                        <version>2.10.4</version>
                        <configuration>
                            <additionalJOptions>
                                <additionalJOption>-Xdoclint:none</additionalJOption>
                            </additionalJOptions>
                        </configuration>
                        <executions>
                            <execution>
                                <phase>package</phase>
                                <goals>
                                    <goal>jar</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <!-- GPG -->
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <version>1.6</version>
                        <executions>
                            <execution>
                                <id>ossrh</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <!-- staging -->
                    <plugin>
                        <groupId>org.sonatype.plugins</groupId>
                        <artifactId>nexus-staging-maven-plugin</artifactId>
                        <version>1.6.7</version>
                        <extensions>true</extensions>
                        <configuration>
                            <serverId>ossrh</serverId>
                            <nexusUrl>https://s01.oss.sonatype.org/</nexusUrl>
                            <autoReleaseAfterClose>false</autoReleaseAfterClose>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

    <!--定义snapshots库和releases库的nexus地址-->
    <distributionManagement>
        <snapshotRepository>
            <id>ossrh</id>
            <name>snapshots</name>
            <url>https://s01.oss.sonatype.org/content/repositories/snapshots</url>
        </snapshotRepository>
        <repository>
            <id>ossrh</id>
            <name>releases</name>
            <url>https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/</url>
        </repository>
    </distributionManagement>
</project>