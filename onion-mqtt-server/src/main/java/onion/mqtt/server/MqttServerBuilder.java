package onion.mqtt.server;

import onion.mqtt.server.auth.IMqttServerConnectHandler;
import onion.mqtt.server.auth.IMqttServerPublishHandler;
import onion.mqtt.server.auth.IMqttServerSubscribeHandler;
import onion.mqtt.server.event.IMqttServerConnectListener;
import onion.mqtt.server.event.IMqttServerMessageListener;
import onion.mqtt.server.event.IMqttServerSubscribeListener;

/**
 * @author Mr, Lu
 * @developmentTeam 浙江允泽信息科技有限公司
 * @createTime 2023/12/12
 */
public class MqttServerBuilder {
    private MqttServerConfig config;
    private IMqttServerConnectHandler connectHandler;
    private IMqttServerSubscribeHandler subscribeHandler;
    private IMqttServerPublishHandler publishHandler;
    private IMqttServerConnectListener connectListener;
    private IMqttServerSubscribeListener subscribeListener;
    private IMqttServerMessageListener messageListener;

    public MqttServerConfig getConfig() {
        return config;
    }

    public MqttServerBuilder config(MqttServerConfig config) {
        this.config = config;
        return this;
    }

    public IMqttServerConnectHandler getConnectHandler() {
        return connectHandler;
    }

    public MqttServerBuilder connectHandler(IMqttServerConnectHandler connectAuth) {
        this.connectHandler = connectAuth;
        return this;
    }

    public IMqttServerSubscribeHandler getSubscribeHandler() {
        return subscribeHandler;
    }

    public MqttServerBuilder subscribeHandler(IMqttServerSubscribeHandler subscribeAcl) {
        this.subscribeHandler = subscribeAcl;
        return this;
    }

    public IMqttServerPublishHandler getPublishHandler() {
        return publishHandler;
    }

    public MqttServerBuilder publishHandler(IMqttServerPublishHandler publishAuth) {
        this.publishHandler = publishAuth;
        return this;
    }

    public IMqttServerConnectListener getConnectListener() {
        return connectListener;
    }

    public MqttServerBuilder connectListener(IMqttServerConnectListener connectListener) {
        this.connectListener = connectListener;
        return this;
    }

    public IMqttServerSubscribeListener getSubscribeListener() {
        return subscribeListener;
    }

    public MqttServerBuilder subscribeListener(IMqttServerSubscribeListener subscribeListener) {
        this.subscribeListener = subscribeListener;
        return this;
    }

    public IMqttServerMessageListener getMessageListener() {
        return messageListener;
    }

    public MqttServerBuilder messageListener(IMqttServerMessageListener messageListener) {
        this.messageListener = messageListener;
        return this;
    }

    public MqttServer build() {
        if (config == null) {
            config = new MqttServerConfig();
        }
        return new MqttServer(this);
    }
}
