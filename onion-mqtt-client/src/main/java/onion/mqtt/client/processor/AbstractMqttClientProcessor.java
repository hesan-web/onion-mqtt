package onion.mqtt.client.processor;

import io.netty.channel.Channel;
import io.netty.handler.codec.mqtt.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Mr, Lu
 * @developmentTeam 浙江允泽信息科技有限公司
 * @createTime 2023/12/13
 */
public abstract class AbstractMqttClientProcessor<T extends MqttMessage> {
    static final Logger log = LoggerFactory.getLogger(AbstractMqttClientProcessor.class);
    /**
     * 处理消息
     *
     * @param channel
     * @param message
     */
    public abstract void process(Channel channel, T message);
}
