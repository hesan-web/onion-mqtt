package onion.mqtt.server.processor;

import io.netty.channel.Channel;
import io.netty.handler.codec.mqtt.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Mr, Lu
 * @developmentTeam 浙江允泽信息科技有限公司
 * @createTime 2023/12/13
 */
public class PubRelProcessor extends AbstractMqttServerProcessor<MqttMessage> {
    static final Logger log = LoggerFactory.getLogger(PubRelProcessor.class);
    @Override
    public void process(Channel channel, MqttMessage message) {
        log.debug("PubRel clientId: {}", getClientId(channel));
        MqttMessageIdVariableHeader variableHeader = (MqttMessageIdVariableHeader) message.variableHeader();
        int messageId = variableHeader.messageId();

        // 发送PubComp
        MqttFixedHeader mqttFixedHeader = new MqttFixedHeader(MqttMessageType.PUBCOMP, false, MqttQoS.AT_MOST_ONCE, false, 0);
        MqttMessage mqttMessage = MqttMessageFactory.newMessage(mqttFixedHeader, MqttMessageIdVariableHeader.from(messageId), null);
        writeAndFlush(channel, mqttMessage);
    }
}
