package onion.mqtt.client;

import onion.mqtt.client.listener.IMqttClientMessageListener;

/**
 * @author Mr, Lu
 * @developmentTeam 浙江允泽信息科技有限公司
 * @createTime 2023/12/16
 */
public class MqttClientBuilder {
    private MqttClientConfig config;
    private IMqttClientMessageListener messageListener;

    public MqttClientBuilder config(MqttClientConfig config) {
        this.config = config;
        return this;
    }

    public MqttClientConfig getConfig() {
        return this.config;
    }

    public MqttClientBuilder messageListener(IMqttClientMessageListener messageListener) {
        this.messageListener = messageListener;
        return this;
    }

    public IMqttClientMessageListener getMessageListener() {
        return this.messageListener;
    }

    public MqttClient build(){
        if (config == null) {
            this.config = new MqttClientConfig();
        }
        return new MqttClient(config);
    }


}
