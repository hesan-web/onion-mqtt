package onion.mqtt.server.store;

import java.io.Serializable;

/**
 * @author Mr, Lu
 * @developmentTeam 浙江允泽信息科技有限公司
 * @createTime 2023/12/12
 */
public class MessageStore implements Serializable {

    private static final long serialVersionUID = 3425139668093307054L;

    /**
     * 客户端Id
     */
    private String clientId;

    /**
     * topic名称
     */
    private String topic;

    /**
     * 消息负载
     */
    private byte[] payload;

    /**
     * 消息等级
     */
    private int qoS;

    /**
     * 保留消息
     */
    private boolean retain;

    /**
     * 消息时间戳
     */
    private long timestamp;

    /**
     * 节点Id
     */
    private String nodeId;

    public MessageStore() {
    }

    public MessageStore(String clientId, String topic, byte[] payload, int qoS, boolean retain, long timestamp, String nodeId) {
        this.clientId = clientId;
        this.topic = topic;
        this.payload = payload;
        this.qoS = qoS;
        this.retain = retain;
        this.timestamp = timestamp;
        this.nodeId = nodeId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public byte[] getPayload() {
        return payload;
    }

    public void setPayload(byte[] payload) {
        this.payload = payload;
    }

    public int getQoS() {
        return qoS;
    }

    public void setQoS(int qoS) {
        this.qoS = qoS;
    }

    public boolean isRetain() {
        return retain;
    }

    public void setRetain(boolean retain) {
        this.retain = retain;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }
}
