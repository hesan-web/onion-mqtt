package onion.mqtt.server.auth;

import io.netty.channel.Channel;

/**
 * 连接认证接口
 *
 * @author Mr, Lu
 * @developmentTeam 浙江允泽信息科技有限公司
 * @createTime 2023/12/12
 */
public interface IMqttServerConnectHandler {

    /**
     * 认证
     * @param channel
     * @param clientId
     * @param username
     * @param password
     * @return
     */
    default boolean verifyAuthenticate(Channel channel, String clientId, String username, String password) {
        try {
            return authenticate(channel, clientId, username, password);
        } catch (Throwable e) {
            return false;
        }
    }

    /**
     * 认证，自定义实现
     * @param channel
     * @param clientId
     * @param username
     * @param password
     * @return
     */
    boolean authenticate(Channel channel, String clientId, String username, String password);
}
