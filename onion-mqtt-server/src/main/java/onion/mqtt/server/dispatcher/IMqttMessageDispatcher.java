package onion.mqtt.server.dispatcher;

import io.netty.channel.Channel;
import onion.mqtt.server.store.MessageStore;

import java.util.List;

/**
 * @author Mr, Lu
 * @developmentTeam 浙江允泽信息科技有限公司
 * @createTime 2023/12/13
 */
public interface IMqttMessageDispatcher {

    /**
     * 处理保留消息分发
     * @param channel
     * @param messageList
     */
    void dispatchRetainMsg(Channel channel, List<MessageStore> messageList);

    /**
     * 处理遗嘱消息分发
     * @param channel
     * @param messageList
     */
    void dispatchWillMsg(Channel channel, List<MessageStore> messageList);
}
